#Opis wykonania zadania.

Do wykonania zadania użyłem frameworku Symfony w wersji 3.4.8.

Aplikacja powstała przy użyciu polecenia:
```
composer create-project symfony/framework-standard-edition nazwa_aplikacji
```

Dodatkowo został użyty panel admina, przy użyciu Sonata Admin. Wybrałem sonatę, ze względu na szeroki zakres możliwości jaki daje oraz możliwości szybkiego stworzenia słowników (i nie tylko).

Komenda do instalacji sonaty:
```
composer require sonata-project/admin-bundle
```

Do komunikacji aplikacji z bazą danych używam ORM.
Używając sonaty, potrzebny jest dodatkowy moduł, opisany w dokumentacji.

```
composer require sonata-project/doctrine-orm-admin-bundle
```

Za pomocą polecenia:

```
./bin/console doctrine:generate:entity
```

utworzyłem Encje odpowiedzialne za obsługę aplikacji z diagramu.

Dodatkowo do encji przy użycia polecenia:

```
./bin/console sonata:admin:generate
```

utworzyłem Adminy (sonatowe) do obsługi encji - taki CRUD.

Adminy te umożliwiają od razu takie funckje jak tworzenie, podgląd, edycja i zapis, lista, usuwanie i export.

Settery i gettery w encjach wygenerowane zostały automatycznie dzięki komendzie:

```
./bin/console doctrine:generate:entities AppBundle:Nazwa_Encji
```

##Wymagania
* PHP >=7.1
* Baza danych PostgreSQL lub MySQL (wymagana zmiana parametru w ```config.yml``` - dostępne są opcje pdo_mysql / pdo_pgsql itp.)

##Instrukcja postępowania

* Aby uruchomią aplikację należy uzyć polecenia ```composer install```
* Po pomyślnym przejściu przez konfigurację, należy uzyć polecenia ```./bin/console doctrine:schema:create```,
aby utworzyć strukturę bazy danych