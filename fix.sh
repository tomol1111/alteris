#!/bin/bash

DIR="src"

echo ""
echo "Php cs fixer"
php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix $DIR --rules=@PSR1,@PSR2,@Symfony
