<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * StuffGroup - grupa materiałów.
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="app_stuff_group")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StuffGroupRepository")
 * @UniqueEntity("name")
 */
class StuffGroup
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="StuffGroup")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="StuffGroup", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="StuffGroup", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @var stuff[]|ArrayCollection - powiązane materiały
     *
     * @ORM\OneToMany(targetEntity="Stuff", mappedBy="group")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $stuffs;

    /**
     * @var string - nazwa
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="250")
     * @ORM\Column(type="string", length=250, unique=true)
     */
    private $name;

    /**
     * StuffGroup constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->stuffs = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lft.
     *
     * @param int $lft
     *
     * @return StuffGroup
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft.
     *
     * @return int
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl.
     *
     * @param int $lvl
     *
     * @return StuffGroup
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl.
     *
     * @return int
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt.
     *
     * @param int $rgt
     *
     * @return StuffGroup
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt.
     *
     * @return int
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root.
     *
     * @param StuffGroup|null $root
     *
     * @return StuffGroup
     */
    public function setRoot(StuffGroup $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root.
     *
     * @return StuffGroup|null
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set parent.
     *
     * @param StuffGroup|null $parent
     *
     * @return StuffGroup
     */
    public function setParent(StuffGroup $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return StuffGroup|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child.
     *
     * @param StuffGroup $child
     *
     * @return StuffGroup
     */
    public function addChild(StuffGroup $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param StuffGroup $child
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeChild(StuffGroup $child)
    {
        return $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add stuff.
     *
     * @param Stuff $stuff
     *
     * @return StuffGroup
     */
    public function addStuff(Stuff $stuff)
    {
        $stuff->setGroup($this);
        $this->stuffs[] = $stuff;

        return $this;
    }

    /**
     * Remove stuff.
     *
     * @param Stuff $stuff
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeStuff(Stuff $stuff)
    {
        return $this->stuffs->removeElement($stuff);
    }

    /**
     * Get stuffs.
     *
     * @return Collection
     */
    public function getStuffs()
    {
        return $this->stuffs;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return StuffGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Pobiera nazwę do prazentacji na liście wyboru.
     *
     * @return string
     */
    public function getNameForList()
    {
        $out = '';

        for ($i = 0; $i < $this->getLvl(); ++$i) {
            $out .= '-';
        }

        $out .= $this->__toString();

        return (string) $out;
    }

    /**
     * Walidacja.
     *
     * @Assert\Callback()
     *
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->getParent() and $this->getParent()->getStuffs()->count()) {
            $context
                ->buildViolation('Can\'t add group to parent group, which have assigned stuffs.')
                ->atPath('parent')
                ->setTranslationDomain('AppBundle')
                ->addViolation();
        }
    }
}
