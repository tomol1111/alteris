<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Stuff - materiał.
 *
 * @ORM\Table(name="app_stuff")
 * @ORM\Entity()
 * @UniqueEntity("code")
 */
class Stuff
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var stuffGroup - grupa meteriałów
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="StuffGroup", inversedBy="stuffs")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $group;

    /**
     * @var unit - jednostka
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Unit")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $unit;

    /**
     * @var string - kod
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="250")
     * @ORM\Column(type="string", length=250, unique=true)
     */
    private $code;

    /**
     * @var string - nazwa
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="250")
     * @ORM\Column(type="string", length=250)
     */
    private $name;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group.
     *
     * @param StuffGroup|null $group
     *
     * @return Stuff
     */
    public function setGroup(StuffGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return StuffGroup|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set unit.
     *
     * @param Unit|null $unit
     *
     * @return Stuff
     */
    public function setUnit(Unit $unit = null)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit.
     *
     * @return Unit|null
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Stuff
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Stuff
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
