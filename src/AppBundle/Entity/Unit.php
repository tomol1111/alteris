<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Unit - jednostka miary.
 *
 * @ORM\Table(name="app_unit")
 * @ORM\Entity()
 * @UniqueEntity("name")
 * @UniqueEntity("short")
 */
class Unit
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string - nazwa
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="250")
     * @ORM\Column(type="string", length=250, unique=true)
     */
    private $name;

    /**
     * @var string - skrócona nazwa
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $short;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName().' ('.$this->getShort().')';
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Unit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set short.
     *
     * @param string $short
     *
     * @return Unit
     */
    public function setShort($short)
    {
        $this->short = $short;

        return $this;
    }

    /**
     * Get short.
     *
     * @return string
     */
    public function getShort()
    {
        return $this->short;
    }
}
