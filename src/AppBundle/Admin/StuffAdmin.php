<?php

namespace AppBundle\Admin;

use AppBundle\Entity\StuffGroup;
use AppBundle\Entity\Unit;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class StuffAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('code')
            ->add('name')
            ->add('group')
            ->add('unit')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('code')
            ->add('name')
            ->add('group')
            ->add('unit')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('code')
            ->add('name')
            ->add('group', EntityType::class, [
                'class' => StuffGroup::class,
                'placeholder' => '',
                'query_builder' => function (EntityRepository $er) {
                    /* @var QueryBuilder $queryBuilder */
                    $queryBuilder = $er->createQueryBuilder('g');

                    $queryBuilder
                        ->orderBy('g.root', 'asc')
                        ->addOrderBy('g.lft', 'asc')
                    ;

                    return $queryBuilder;
                },
                'choice_label' => function ($val) {
                    /* @var StuffGroup $val */
                    return (string) $val->getNameForList();
                },
                'choice_attr' => function ($val) {
                    /* @var StuffGroup $val */
                    return ['disabled' => (bool) $val->getChildren()->count()];
                },
            ])
            ->add('unit', ModelType::class, [
                'required' => true,
                'class' => Unit::class,
                'btn_add' => true,
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('code')
            ->add('name')
            ->add('group', null, [
                'route' => [
                    'name' => 'show',
                ],
            ])
            ->add('unit', null, [
                'route' => [
                    'name' => 'show',
                ],
            ])
        ;
    }
}
