<?php

namespace AppBundle\Admin;

use AppBundle\Entity\StuffGroup;
use AppBundle\Repository\StuffGroupRepository;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class StuffGroupAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'asc',
        '_sort_by' => 'lft',
    ];

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        /** @var StuffGroup $object */
        $object = parent::getNewInstance();

        if ($this->request->query->has('parent')) {
            //jeśli w parametrach przekazany jest parent, ustawiamy go
            /** @var EntityManager $em */
            $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
            $parent = $em->getRepository(StuffGroup::class)->find($this->request->query->getInt('parent'));
            if ($parent) {
                $object->setParent($parent);
            }
        }

        return $object;
    }

    /**
     * Pobranie drzewa.
     *
     * @param null $object
     *
     * @return array|string
     */
    public function getTree($object = null)
    {
        /** @var EntityManager $em */
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        /** @var StuffGroupRepository $stuffGroupRepository */
        $stuffGroupRepository = $em->getRepository(StuffGroup::class);

        $qb = $stuffGroupRepository
            ->createQueryBuilder('sg')
            ->orderBy('sg.lft', 'ASC')
        ;

        $elements = $qb->getQuery()->getResult();
        $items = [];

        /** @var StuffGroup $element */
        foreach ($elements as $element) {
            $items[$element->getId()] = $element;
        }

        $options = [
            'decorate' => true,
            'rootOpen' => '<ul>',
            'rootClose' => '</ul>',
            'childOpen' => '<li>',
            'childClose' => '</li>',
            'nodeDecorator' => function ($node) use ($items) {
                /** @var StuffGroup $node */
                $out = '<a '
                    .'data-id="'.$node['id'].'" '
                    .'data-disabled="'.((bool) $items[$node['id']]->getStuffs()->count()).'" '
                    .'href="'.$this->generateUrl('show', ['id' => $node['id']]).'">';

                $out .= (string) $items[$node['id']];
                $out .= '</a>';

                return $out;
            },
        ];

        return $stuffGroupRepository->childrenHierarchy($object, false, $options);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('parent')
            ->add('name')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('parent', null, [
                'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p');
                    $alias = $qb->getRootAliases()[0];

                    $qb
                        ->orderBy('p.root', 'asc')
                        ->addOrderBy('p.lft', 'asc')
                    ;

                    if ($this->getSubject() and $this->getSubject()->getId()) {
                        //jeśli edytujemy obiekt nie można ustawić go jako parenta
                        $qb
                            ->andWhere($alias.'.id != :object')
                            ->setParameter('object', $this->getSubject())
                        ;
                    }

                    return $qb;
                },
                'choice_attr' => function ($val) {
                    /** @var StuffGroup $val */
                    if ($val === $this->getSubject()->getParent()) {
                        return [];
                    }

                    return ['disabled' => (bool) ($val->getChildren()->count() or $val->getStuffs()->count())];
                },
                'choice_label' => function ($val) {
                    /* @var StuffGroup $val */
                    return $val->getNameForList();
                },
            ])
            ->add('name')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('parent', null, [
                'route' => [
                    'name' => 'show',
                ],
            ])
            ->add('children', null, [
                'route' => [
                    'name' => 'show',
                ],
            ])
            ->add('stuffs', null, [
                'route' => [
                    'name' => 'show',
                ],
            ])
        ;
    }
}
