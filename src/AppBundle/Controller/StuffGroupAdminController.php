<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;

class StuffGroupAdminController extends CRUDController
{
    /**
     * Nadpisana akcja listy, uwzględniająca drzewo.
     *
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if (null !== $preResponse) {
            return $preResponse;
        }

        $htmlTree = $this->admin->getTree();

        return $this->renderWithExtraParams('@App/StuffGroupAdmin/tree.html.twig', [
            'action' => 'list',
            'htmlTree' => $htmlTree,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            '_sonata_admin' => $request->get('_sonata_admin'),
        ], null, $request);
    }
}
